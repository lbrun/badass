<?php

namespace App\Commands;

use App\Classes\Command;
use App\Classes\Env;
use App\Classes\Config;
use App\Classes\Shout;
use App\Helpers\StringHelper;
use App\Repositories\GitlabRepository;

class IssueStart extends Command
{
	public static $position = 21;
	public static $verboseName = 'issue start';
	public static $verboseUsage = 'issue start <id> <branch>';
	public static $verboseDescription = 'Create a new branch and change issue status.';
	public static $verboseArguments = ['id' => 'Id of the issue', 'branch' => 'Name of the source branch'];
	public static $verboseOptions = [];

	private $_issue;
	private $_authId;
	private $_source;

	public function parse()
	{
		if (count($this->_arguments) != 2)
			$this->printUsage();

		$id = $this->_arguments[0];
		if (!is_numeric($id))
			$this->_addError('"' . $id . '" is not a valid issue id.', true);

		$this->_source = $this->_arguments[1];

		Shout::yellow('Retrieving issue...')->next();

		$url = 'projects/' . Env::get('PROJECT_ID') . '/issues/' . $id;
		$response = GitlabRepository::instance()->get($url);

		if ($response['status'] === false) {

			if ($response['code'] === 404)
				$this->_addError('"' . $id . '" is not a valid issue id.', true);
			else
				$this->_addError($response['message'] . '. ' . $response['error'], true);
		}

		$this->_issue = $response['data'];

		if ($this->_issue->state != Config::get('issues.status.opened')
			|| count(array_intersect(Config::get('issues.labels.status'), $this->_issue->labels)))
			$this->_addError('This issue is not available.', true);

		$this->_authId = GitlabRepository::instance()->getAuthId($this);
		if ($this->_issue->assignee != null && $this->_issue->assignee->id != $this->_authId)
			$this->_addError('This issue is assigned to someone else.', true);

		Shout::text('Retrieving issue succeed.')->jump();

		$this->_source = $this->_arguments[1] ?: 'master';
	}

	public function execute()
	{
		if (in_array(Config::get('issues.labels.types.feature'), $this->_issue->labels))
			$type = Config::get('issues.prefix.feature');
		else if (in_array(Config::get('issues.labels.types.bug'), $this->_issue->labels))
			$type = Config::get('issues.prefix.bug');
		else if (in_array(Config::get('issues.labels.types.optimization'), $this->_issue->labels))
			$type = Config::get('issues.prefix.optimization');
		else
			$type = Config::get('issues.prefix.misc');

		$slug = StringHelper::slugify($type . '-' . $this->_issue->iid . '-' . $this->_issue->title);

		Shout::yellow('Creating new ' . $type . ' branch...')->next();
		$this->_createBranch($slug, $this->_source);
		Shout::text('Branch "' . $slug . '" created successfully.')->jump();

		Shout::yellow('Updating issue...')->next();
		$this->_updateIssue();
		Shout::text('Issue updated successfully.')->jump();

		Shout::yellow('Fetching remote branches...')->next();
		exec('git fetch', $output, $return);
		if ($return != 0)
		{
			$this->_addError('Failed fetching remote branches. Fix errors and run: git fetch');
			Shout::next();
		}
		else
			Shout::text('Remote branches fetched successfully.')->jump();

		Shout::yellow('Switching to new branch...')->next();
		exec('git checkout ' . $slug, $output, $return);
		if ($return != 0)
		{
			$this->_addError('Failed switching to new branch. Fix errors and run: git checkout ' . $slug);
			Shout::next();
		}
		else
			Shout::text('Branch switched successfully.')->jump();

		Shout::green('Start issue succeed');
		empty($this->_errors)
			? Shout::green('.')->jump()
			: Shout::red(' with ' . count($this->_errors) . ' error(s).')->jump();
	}

	private function _createBranch($branchName, $source)
	{
		$url = 'projects/' . Env::get('PROJECT_ID') . '/repository/branches';
		$response = GitlabRepository::instance()->post($url, [
			'branch' => $branchName,
			'ref' => $source
		]);

		if ($response['status'] === false)
			$this->_addError($response['message'] . '. ' . $response['error'], true);
	}

	private function _updateIssue()
	{
		$labels = $this->_issue->labels;
		$labels[] = Config::get('issues.labels.status.working');

		$url = 'projects/' . Env::get('PROJECT_ID') . '/issues/' . $this->_issue->iid;
		$response = GitlabRepository::instance()->put($url, [
			'assignee_ids' => $this->_authId,
			'labels' => implode(',', $labels)
		]);

		if ($response['status'] === false)
			$this->_addError($response['message'] . '. ' . $response['error'], true);
	}
}