<?php

namespace App\Commands;

use App\Classes\Command;
use App\Classes\Env;
use App\Classes\Config;
use App\Classes\Shout;
use App\Helpers\StringHelper;
use App\Repositories\GitlabRepository;

class IssueFinish extends Command
{
	public static $position = 22;
	public static $verboseName = 'issue finish';
	public static $verboseUsage = 'issue finish <target>';
	public static $verboseDescription = 'Create the MR and change issue status.';
	public static $verboseArguments = ['target' => 'Target branch'];

	private $_issue;
	private $_branch;
	private $_target;
	private $_authId;

	public function parse()
	{
		if (count($this->_arguments) != 1)
			$this->printUsage();

		Shout::yellow('Retrieving current branch...')->next();

		exec("git branch | grep \*", $output, $return);
		if ($return != 0)
			$this->_addError("You need to execute this command on the corresponding issue branch.", true);

		$this->_branch = substr($output[0], 2);

		$explode = explode('-', $this->_branch);
		if (count($explode) < 2)
			$this->_addError("The current branch is not a valid issue branch.", true);

		$issueId = $explode[1];

		Shout::text("Current branch retrieved successfully.")->jump();

		Shout::yellow('Retrieving issue...')->next();

		$url = 'projects/' . Env::get('PROJECT_ID') . '/issues/' . $issueId;
		$response = GitlabRepository::instance()->get($url);

		if ($response['status'] === false) {

			if ($response['code'] === 404)
				$this->_addError("The current branch is not a valid issue branch.", true);
			else
				$this->_addError($response['message'] . '. ' . $response['error'], true);
		}

		$this->_issue = $response['data'];

		if (!in_array(Config::get('issues.labels.status.working'), $this->_issue->labels))
			$this->_addError("This issue branch is not in working status.", true);

		$url = 'projects/' . Env::get('PROJECT_ID') . '/issues/' . $issueId . '/closed_by';
		$response = GitlabRepository::instance()->get($url);

		if ($response['status'] === false)
			$this->_addError($response['message'] . '. ' . $response['error'], true);

		if (!empty($response['data']))
			$this->_addError('This issue branch already have a MR.', true);

		$this->_authId = GitlabRepository::instance()->getAuthId($this);
		if ($this->_issue->assignee != null && $this->_issue->assignee->id != $this->_authId)
			$this->_addError('This issue branch is assigned to someone else.', true);

		Shout::text('Retrieving issue succeed.')->jump();

		$this->_target = $this->_arguments[0];
	}

	public function execute()
	{
		Shout::yellow('Creating new merge request...')->next();
		$this->_createMergeRequest();
		Shout::text('Merge request created successfully.')->jump();

		Shout::yellow('Updating issue...')->next();
		$this->_updateIssue();
		Shout::text('Issue updated successfully.')->jump();

		Shout::green('Finish issue succeed.')->jump();
	}

	private function _createMergeRequest()
	{
		$url = 'projects/' . Env::get('PROJECT_ID') . '/merge_requests';
		$response = GitlabRepository::instance()->post($url, [
			'source_branch' => $this->_branch,
			'target_branch' => $this->_target,
			'title' => $this->_issue->title,
			'description' => 'References #' . $this->_issue->iid,
			'remove_source_branch' => true,
			'squash' => true
		]);

		if ($response['status'] === false)
			$this->_addError($response['message'] . '. ' . $response['error'], true);
	}

	private function _updateIssue()
	{
		$labels = $this->_issue->labels;
		unset($labels[array_search(Config::get('issues.labels.status.working'), $labels)]);
		$labels[] = Config::get('issues.labels.status.review');

		$url = 'projects/' . Env::get('PROJECT_ID') . '/issues/' . $this->_issue->iid;
		$response = GitlabRepository::instance()->put($url, [
			'labels' => implode(',', $labels)
		]);

		if ($response['status'] === false)
			$this->_addError($response['message'] . '. ' . $response['error'], true);
	}
}