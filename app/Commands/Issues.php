<?php

namespace App\Commands;

use App\Classes\Command;
use App\Classes\Env;
use App\Classes\Shout;
use App\Classes\Config;
use App\Repositories\GitlabRepository;

class Issues extends Command
{
	public static $position = 20;
	public static $verboseName = 'issues';
	public static $verboseUsage = 'issues';
	public static $verboseDescription = 'List all the availables issues.';
	public static $verboseOptions = [
		'b' => 'Display only bug issues',
		'f' => 'Display only feature issues',
		'o' => 'Display only optimization issues'
	];

	public function parse()
	{
		if (count($this->_arguments) > 0)
			$this->printUsage();
	}

	public function execute()
	{
		$url = 'projects/' . Env::get('PROJECT_ID') . '/issues';
		$response = GitlabRepository::instance()->get($url, [
			'status' => Config::get('issues.status.opened'),
			'labels' => Config::get('issues.labels.status.todo'),
		]);

		if ($response['status'] === false)
			$this->_addError($response['message'] . '. ' . $response['error'], true);

		$issues = $response['data'];

		if ($this->hasAtLeastOneOption(['f', 'b', 'o']))
			$issues = $this->_filterOptions($issues);

		$issues = $this->_filterAssignees($issues);

		$this->_printIssues($issues);
	}

	private function _filterOptions($issues)
	{
		return array_filter($issues, function ($issue) {
			if ($this->hasOption('f') && in_array(Config::get('issues.labels.types.feature'), $issue->labels))
				return true;

			if ($this->hasOption('b') && in_array(Config::get('issues.labels.types.bug'), $issue->labels))
				return true;

			if ($this->hasOption('o') && in_array(Config::get('issues.labels.types.optimization'), $issue->labels))
				return true;

			return false;
		});
	}

	private function _filterAssignees($issues)
	{
		$authId = GitlabRepository::instance()->getAuthId($this);

		return array_filter($issues, function ($issue) use ($authId) {
			return !($issue->assignee != null && $issue->assignee->id != $authId);
		});
	}

	private function _printIssues($issues)
	{
		$spaces = 0;
		foreach ($issues as $issue)
		{
			if (strlen($issue->iid) + 1 > $spaces)
				$spaces = strlen($issue->iid) + 1;
		}

		Shout::green('Availables Issues ')->text('(')->yellow(count($issues))->text(')')->jump();

		foreach ($issues as $issue)
		{
			Shout::yellow('#' . $issue->iid);

			$delta = $spaces - strlen($issue->iid);
			for ($i = 0; $i < $delta; ++$i)
				Shout::text(' ');

			if (in_array(Config::get('issues.labels.types.feature'), $issue->labels))
				Shout::blue('[FEA] ');
			else if (in_array(Config::get('issues.labels.types.bug'), $issue->labels))
				Shout::red('[BUG] ');
			else if (in_array(Config::get('issues.labels.types.optimization'), $issue->labels))
				Shout::yellow('[OPT] ');
			else
				Shout::yellow('[OTH] ');

			Shout::text($issue->title)->next();
		}

		if (count($issues) > 0)
			Shout::next();
	}
}