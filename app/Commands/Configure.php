<?php

namespace App\Commands;

use App\Classes\Command;
use App\Classes\Env;
use App\Classes\Shout;
use App\Classes\Questioner;
use App\Repositories\GitlabRepository;

class Configure extends Command
{
	public static $position = 12;
	public static $verboseName = 'configure';
	public static $verboseUsage = 'configure';
	public static $verboseDescription = 'Configure badass.';
	public static $verboseOptions = ['test' => 'Test connection with project.'];

	private $_command;

	public function parse()
	{
		if (count($this->_arguments) != 0)
			$this->printUsage();
	}

	public function execute()
	{
		if ($this->hasOption('test')) {
			$this->_testConnection();
			return;
		}

		$result = Questioner::ask('Gitlab url ?');
		if ($result != '')
			$this->_setGitlabUrl($result);
		else
			Shout::yellow('Leaving gitlab url as current value.')->next();

		$result = Questioner::ask('User access token ?');
		if ($result != '')
			$this->_setAccessToken($result);
		else
			Shout::yellow('Leaving access token as current value.')->next();

		$result = Questioner::ask('Project id ?');
		if ($result != '')
			$this->_setProjectId($result);
		else
			Shout::yellow('Leaving project id as current value.')->next();

		$result = Questioner::ask('Would you like to test connection ? [yes]');
		if ($result === 'yes' || $result === 'y' || $result === '')
			$this->_testConnection();
		else
			Shout::yellow('Test connection is canceled.')->jump();
	}

	private function _setGitlabUrl($url)
	{
		$save = Env::get('API_URL');
		Env::set('API_URL', $url);

		Shout::yellow('Fetching gitlab url...')->next();
		$response = GitlabRepository::instance()->get('');

		if ($response['status'] === false && $response['code'] !== 404) {
			Env::set('API_URL', $save);
			$this->_addError($response['message'] . '. ' . $response['error'], true);
		}

		Shout::green('Gitlab url has been set successfully.')->next();
	}

	private function _setAccessToken($accessToken)
	{
		$save = Env::get('API_TOKEN');
		Env::set('API_TOKEN', $accessToken);

		Shout::yellow('Fetching user...')->next();
		$response = GitlabRepository::instance()->get('user');

		if ($response['status'] === false) {
			Env::set('API_TOKEN', $save);
			$this->_addError('"' . $accessToken . '" is not a valid access token.', true);
		}

		Shout::green('You have successfully logged in as ' . $response['data']->username . '.')->next();
	}

	private function _setProjectId($id)
	{
		Shout::yellow('Fetching project...')->next();

		$url = 'projects/' . $id;
		$response = GitlabRepository::instance()->get($url);

		if ($response['status'] === false) {

			if ($response['code'] === 404)
				$this->_addError('"' . $id . '" is not a valid project id.', true);
			else
				$this->_addError($response['message'] . '. ' . $response['error'], true);
		}

		Env::set('PROJECT_ID', $id);
		Shout::green('Project id has been set successfully.')->next();
	}

	private function _testConnection()
	{
		Shout::yellow('Testing connection...')->next();

		$url = 'projects/' . Env::get('PROJECT_ID');
		$response = GitlabRepository::instance()->get($url);

		if ($response['status'] === false)
			$this->_addError($response['message'] . '. ' . $response['error'], true);

		Shout::green('The connection test was successful.')->jump();
	}
}