<?php

namespace App\Commands;

use App\Classes\Command;
use App\Classes\Env;
use App\Classes\Shout;
use App\Repositories\GitlabRepository;
use App\Helpers\CommandParser;

class Evolve extends Command
{
	public static $position = 11;
	public static $verboseName = 'evolve';
	public static $verboseUsage = 'evolve';
	public static $verboseDescription = 'Check and install badass updates.';
	public static $verboseArguments = [];
	public static $verboseOptions = [];

	public function parse()
	{
		if (count($this->_arguments) != 0)
			$this->printUsage();
	}

	public function execute()
	{
		exec('(cd ' . __ROOT . ')', $output, $return);
		if ($return != 0)
			$this->_addError("Badass cannot check for updates: Project not found.", true);

		exec('(cd ' . __ROOT . ' && git branch | grep \*)', $output, $return);
		if ($return != 0)
			$this->_addError("Badass cannot check for updates: Git is missing.", true);

		Shout::yellow('Checking for updates...')->next();

		exec('(cd ' . __ROOT . ' && git remote update && git status -uno)', $status, $return);
		if ($return != 0)
			$this->_addError("Badass failed checking updates.", true);

		$upToDate = false;
		foreach ($status as $line)
		{
			if (strpos($line, 'up-to-date') !== FALSE) {
				$upToDate = true;
				break;
			}
		}

		if ($upToDate) {
			Shout::text('No updates has been found.')->jump();
			Shout::green('Badass is already up-to-date')->jump();
			return;
		}

		Shout::text('Some updates has been found.')->jump();

		Shout::yellow('Installing updates...')->next();

		exec('(cd ' . __ROOT . ' && git pull)', $output, $return);
		if ($return != 0)
			$this->_addError("Badass failed installing updates.", true);

		Shout::green('Updates installed successfully.')->jump();
	}
}