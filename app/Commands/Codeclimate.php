<?php

namespace App\Commands;

use App\Classes\Command;
use App\Classes\Shout;

class Codeclimate extends Command
{
	public static $position = 40;
	public static $verboseName = 'codeclimate';
	public static $verboseUsage = 'codeclimate';
	public static $verboseDescription = 'Analyze code using codeclimate to detect dirty code.';
	public static $verboseOptions = [
		'debug' => 'Display the codeclimate engines debug log',
		'validate' => 'Validate your codeclimate configuration'
	];

	private $_command;

	public function parse()
	{
		if (count($this->_arguments) > 0)
			$this->printUsage();

		$this->_command = 'docker run ';

		if (!$this->hasOption('validate') && $this->hasOption('debug'))
			$this->_command .= '--env CODECLIMATE_DEBUG=1 ';

		$this->_command .= '--env CODECLIMATE_CODE="$PWD" ' .
			'--volume "$PWD":/code ' .
			'--volume /var/run/docker.sock:/var/run/docker.sock ' .
			'--volume /tmp/cc:/tmp/cc ' .
			'codeclimate/codeclimate ';

		$this->_command .= $this->hasOption('validate') ? 'validate-config' : 'analyze -f html > /tmp/codeclimate.html';
	}

	public function execute()
	{
		if ($this->hasOption('validate'))
		{
			$this->_validate();
			return ;
		}

		$this->_analyze();
	}

	private function _validate()
	{
		Shout::yellow('Validating configuration...')->next();

		exec($this->_command, $output, $return);

		$color = $return != 0 ? 'red' : 'green';

		foreach ($output as $line)
			Shout::$color($line)->jump();
	}

	private function _analyze()
	{
		Shout::yellow('Analyzing code...')->next();

		exec($this->_command, $output, $return);

		if ($return != 0)
			$this->_addError('Failed validating code.', true);

		Shout::text("Analyzing code end successfully.")->jump();

		exec('open /tmp/codeclimate.html', $output, $return);

		Shout::green('Opening report at /tmp/codeclimate.html')->jump();
	}
}