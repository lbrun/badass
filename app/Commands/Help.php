<?php

namespace App\Commands;

use App\Classes\Command;
use App\Classes\Env;
use App\Classes\Shout;
use App\Repositories\GitlabRepository;
use App\Helpers\CommandParser;

class Help extends Command
{
	public static $position = 10;
	public static $verboseName = 'help';
	public static $verboseUsage = 'help <command>';
	public static $verboseDescription = 'Print a command usage.';
	public static $verboseArguments = ['command' => 'Name of the command'];

	private $_command;

	public function parse()
	{
		if (count($this->_arguments) == 0)
			$this->printUsage();

		$this->_command = CommandParser::instance()->detectCommand($this->_arguments);
		if ($this->_command == null)
			$this->_addError("This command doesn't exists.", true);
	}

	public function execute()
	{
		$this->_command->printUsage();
	}
}