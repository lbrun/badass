<?php

namespace App\Commands;

use App\Classes\Command;
use App\Classes\Env;
use App\Classes\Shout;
use App\Repositories\GitlabRepository;
use App\Helpers\CommandParser;

class CleanBranches extends Command
{
	public static $position = 30;
	public static $verboseName = 'clean branches';
	public static $verboseUsage = 'clean branches';
	public static $verboseDescription = 'Remove the untracked local branches.';
	public static $verboseArguments = [];
	public static $verboseOptions = [];

	public function parse()
	{
		if (count($this->_arguments) != 0)
			$this->printUsage();
	}

	public function execute()
	{
		exec("git branch | grep \*", $output, $return);
		if ($return != 0)
			$this->_addError("You need to be in a git repository.", true);

		if (substr($output[0], 2) != 'master')
			$this->_addError("You need to be on master branch to run this command.", true);

		Shout::yellow('Fetching remote branches...')->next();
		exec('git fetch -p', $output, $return);
		if ($return != 0)
			$this->_addError('Failed fetching remote branches.', true);

		Shout::text('Remote branches fetched successfully.')->jump();

		Shout::yellow('Cleaning untracked local branches...')->next();
		exec('for branch in `git branch -vv | grep ": gone]" | awk \'{print $1}\'` ; do git branch -D $branch ; done');
		Shout::text('Untracked local branches cleaned successfully.')->jump();

		Shout::green('Cleaning branches succeed.')->jump();
	}
}