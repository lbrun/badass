<?php

namespace App\Commands;

use App\Classes\Command;
use App\Classes\Shout;

class Tests extends Command
{
	public static $position = 50;
	public static $verboseName = 'tests';
	public static $verboseUsage = 'tests [path]';
	public static $verboseDescription = 'List all the tests in project.';

	private $_path;

	public function parse()
	{
		if (count($this->_arguments) > 1) {
			$this->printUsage();
		}

		$this->_path = $this->_arguments[0] ?? '.';
	}

	public function execute()
	{
		exec('cd ' . $this->_path . ' && vendor/bin/phpunit --list-tests', $output, $return);
		if ($return != 0) {
			$this->_addError('Failed listing tests in "' . $this->_path . '" directory.', true);
		}

		$callback = function($elem) {
			if (substr($elem, 0, 8) !== ' - Tests') {
				return null;
			}

			return substr($elem, 9);
		};

		$results = array_filter(array_map($callback, $output));

		$tests = $this->_sortTests($results);

		$this->_displayTests($tests);
	}

	private function _sortTests($results)
	{
		$tests = [];
		foreach ($results as $result) {
			$fields = explode('::', $result, 2);
			$tests[str_replace('\\', ' - ', $fields[0])][] = $fields[1];
		}

		return $tests;
	}

	private function _displayTests($tests)
	{
		foreach ($tests as $path => $list) {
			Shout::next()->yellow($path . ':')->next();

			foreach ($list as $test) {
				Shout::text('    ')->text($test)->next();
			}
		}
	}
}