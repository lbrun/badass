<?php

namespace App\Repositories;

use \Curl\Curl;
use App\Classes\Env;
use App\Classes\Config;

class GitlabRepository
{
	public static $instance;

	private static $_BASE_URL;
	private $_curl;

	public static function instance()
	{
		if (static::$instance == null)
			static::$instance = new GitlabRepository();

		return static::$instance;
	}

	public function __construct()
	{
		static::$_BASE_URL = Env::get('API_URL') . '/api/v4/';

		$this->_curl = new Curl();
		$this->_curl->setHeader('PRIVATE-TOKEN', Env::get('API_TOKEN'));
	}

	public function get($url, $parameters = [])
	{
		$this->_curl->get(static::$_BASE_URL . $url, $parameters);

		if ($this->_curl->error)
			return ['status' => false, 'code' => $this->_curl->errorCode, 'message' => $this->_curl->errorMessage, 
				'error' => $this->_curl->response->message  ?? null];

		return ['status' => true, 'data' => $this->_curl->response];
	}

	public function post($url, $parameters = [])
	{
		$this->_curl->post(static::$_BASE_URL . $url, $parameters);

		if ($this->_curl->error)
			return ['status' => false, 'code' => $this->_curl->errorCode, 'message' => $this->_curl->errorMessage, 
				'error' => $this->_curl->response->message ?? null];

		return ['status' => true, 'data' => $this->_curl->response];
	}

	public function put($url, $parameters = [])
	{
		$this->_curl->put(static::$_BASE_URL . $url, $parameters);

		if ($this->_curl->error)
			return ['status' => false, 'code' => $this->_curl->errorCode, 'message' => $this->_curl->errorMessage, 
				'error' => $this->_curl->response->message ?? null];

		return ['status' => true, 'data' => $this->_curl->response];
	}

	public function getAuthId($command)
	{
		$response = GitlabRepository::instance()->get('user');

		if ($response['status'] === false)
			$command->_addError($response['message'] . '. ' . $response['error'], true);

		return $response['data']->id;
	}

}