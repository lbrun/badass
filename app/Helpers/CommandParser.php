<?php

namespace App\Helpers;

class CommandParser
{
	public static $instance;

	private $_commands = [];

	public static function instance()
	{
		if (static::$instance == null)
			static::$instance = new CommandParser();

		return static::$instance;
	}

	public function setCommands($commands)
	{
		$this->_commands = $commands;
	}

	public function detectCommand($arguments)
	{
		$cmdName = null;
		$j = 0;
		for ($i = 0; $i < count($arguments); ++$i)
		{
			$string = implode(' ', array_slice($arguments, 0, $i + 1));

			if (!array_key_exists($string, $this->_commands))
				continue;

			$cmdName = $string;
			$j = $i + 1;
		}

		if ($cmdName == null)
			return null;

		$cmdArguments = [];
		$cmdOptions = [];
		for ($j; $j < count($arguments); ++$j)
		{
			if (substr($arguments[$j], 0, 1) === '-' && substr($arguments[$j], 1, 1) !== '-')
			{
				$cmdOptions = array_merge($cmdOptions, str_split(substr($arguments[$j], 1)));
				continue;
			}

			if (substr($arguments[$j], 0, 2) === '--')
			{
				$cmdOptions[] = substr($arguments[$j], 2);
				continue;
			}

			$cmdArguments[] = $arguments[$j];
		}
		
		return new $this->_commands[$cmdName](array_filter($cmdArguments), array_filter($cmdOptions));
	}

}