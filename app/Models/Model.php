<?php

namespace App\Models;

class Model
{
	protected $_attributes = [];

	public function __get($name)
    {
        if (array_key_exists($name, $this->_attributes))
            return $this->_attributes[$name];

        return null;
    }

    public function __set($name, $value)
    {
    	$this->_attributes[$name] = $value;
        return $this;
    }
}