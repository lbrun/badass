<?php

namespace App\Classes;

abstract class Command
{
	public static $position = -1;

	public static $verboseName = 'command';
	public static $verboseUsage = 'command';
	public static $verboseDescription = 'Missing command description';
	public static $verboseArguments = [];
	public static $verboseOptions = [];
	public static $version = '1.0';

	protected $_arguments = [];
	protected $_options = [];
	protected $_errors = [];

	abstract public function parse();
	abstract public function execute();

	public function __construct($arguments = [], $options = [])
	{
		$this->_arguments = $arguments;
		$this->_options = $options;
	}

	protected function _addError($error, $stopPropagation = false)
	{
		$this->_errors[] = $error;

		if ($stopPropagation)
			$this->printErrors();
	}

	public function printErrors()
	{
		if (empty($this->_errors))
			return ;

		Shout::next()->yellow('Errors:')->next();
		foreach ($this->_errors as $error)
			Shout::red('  ' . $error)->next();

		Shout::next();
		die();
	}

	public function printUsage()
	{
		Shout::blue(ucwords(static::$verboseName) . ' Command')->text(' version ')->green(static::$version)->next();
		Shout::text(static::$verboseDescription)->jump();

		Shout::yellow('Usage:')->next();
		Shout::text('  ' . static::$verboseUsage)->jump();

		if (!empty(static::$verboseArguments))
			$this->_printArguments();

		if (!empty(static::$verboseOptions))
			$this->_printOptions();

		die();
	}

	protected function hasOption(string $option)
	{
		return in_array($option, $this->_options);
	}

	protected function hasOptions(array $options)
	{
		return count(array_intersect($this->_options, $options)) === count($options);
	}

	protected function hasAtLeastOneOption(array $options)
	{
		return count(array_intersect($this->_options, $options)) > 0;
	}

	private function _printArguments()
	{
		$deltaSpace = 0;
    	foreach(static::$verboseArguments as $argument => $description)
    		$deltaSpace = $deltaSpace < strlen($argument) ? strlen($argument) : $deltaSpace;

    	$deltaSpace = $deltaSpace + 6;

		Shout::yellow('Arguments:')->next();
		foreach (static::$verboseArguments as $argument => $description)
		{
			Shout::green('  ' . $argument);
			for($i = strlen($argument); $i < $deltaSpace; ++$i)
				Shout::text(' ');
			Shout::text($description)->next();
		}

		Shout::next();
	}

	private function _printOptions()
	{
		$deltaSpace = 0;
    	foreach(static::$verboseOptions as $option => $description)
    		$deltaSpace = $deltaSpace < strlen($option) ? strlen($option) : $deltaSpace;

    	$deltaSpace = $deltaSpace + 6;

		Shout::yellow('Options:')->next();
		foreach (static::$verboseOptions as $option => $description)
		{
			Shout::green('  ' . (strlen($option) > 1 ? '--' : '-') . $option);
			for($i = strlen($option); $i < $deltaSpace; ++$i)
				Shout::text(' ');
			Shout::text($description)->next();
		}

		Shout::next();
	}
}