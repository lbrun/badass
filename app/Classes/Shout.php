<?php

namespace App\Classes;

class Shout
{
	const RESET_COLOR = "\033[0m";
	const RED_COLOR = "\033[31m";
	const GREEN_COLOR = "\033[32m";
	const YELLOW_COLOR = "\033[33m";
	const BLUE_COLOR = "\033[34m";

	private static $instance;

	public static function instance()
	{
		if (self::$instance != null)
			return self::$instance;

		self::$instance = new Shout();
		return self::$instance;
	}

	public static function __callStatic($method, $arguments)
    {
    	$method = '_' . $method;

    	if (!method_exists(self::instance(), $method))
    		die();

    	call_user_func_array([self::instance(), $method], $arguments);
    	return self::instance();
    }

    public function __call($method, $arguments)
    {
    	$method = '_' . $method;

    	if (!method_exists($this, $method))
    		die();

    	call_user_func_array([$this, $method], $arguments);
    	return $this;
    }

    private function _red($string)
	{
		echo Shout::RED_COLOR . $string . Shout::RESET_COLOR;
	}

	private function _blue($string)
	{
		echo Shout::BLUE_COLOR . $string . Shout::RESET_COLOR;
	}

	private function _yellow($string)
	{
		echo Shout::YELLOW_COLOR . $string . Shout::RESET_COLOR;
	}

	private function _green($string)
	{
		echo Shout::GREEN_COLOR . $string . Shout::RESET_COLOR;
	}

	private function _debug($string)
	{
		echo PHP_EOL . PHP_EOL . Shout::RED_COLOR . $string . Shout::RESET_COLOR . PHP_EOL . PHP_EOL;
	}

	private function _text($string)
	{
		echo $string;
	}

	private function _next()
	{
		echo PHP_EOL;
	}

	private function _jump()
	{
		echo PHP_EOL . PHP_EOL;
	}
	
}