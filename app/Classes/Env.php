<?php

namespace App\Classes;

class Env
{
	private static $_ENV_FILE = __ROOT . '/.env';

	public static function get($name)
    {
    	$vars = static::_parse();
    	return isset($vars[$name]) ? $vars[$name] : null;
    }

    public static function set($key, $value)
    {
        $vars = static::_parse();
        $vars[$key] = $value;
        static::_save($vars);
    }

    private static function _parse()
    {
    	if (!file_exists(static::$_ENV_FILE))
    		return [];

    	$contents = file_get_contents(static::$_ENV_FILE);
    	$lines = array_filter(explode(PHP_EOL, $contents));

    	$vars = [];
    	foreach ($lines as $line)
    	{
    		list($key, $value) = explode('=', $line, 2);
    		$vars[$key] = $value;
    	}

    	return $vars;
    }

    private static function _save($vars)
    {
        $content = '';
        foreach ($vars as $key => $value)
            $content.= $key . '=' . $value . PHP_EOL;

        file_put_contents(static::$_ENV_FILE, $content);
    }
}