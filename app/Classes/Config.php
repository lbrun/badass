<?php

namespace App\Classes;

class Config
{
	private static $_CONFIG_PATH = __ROOT . 'config/';

	public static function get($name)
    {
        $path = explode('.', $name);

        if (count($path) < 2)
            return null;

        $config = static::getConfigFromFile($path[0]);

        for ($i = 1; $i < count($path); ++$i)
        {
            if (!isset($config[$path[$i]]))
                return null;

            $config = $config[$path[$i]];
        }

        return $config;
    }

    private static function getConfigFromFile($file)
    {
        $filename = static::$_CONFIG_PATH . $file . '.php';
        if (!file_exists($filename))
            return null;

        return include $filename;
    }
}