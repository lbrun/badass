<?php

namespace App\Classes;

use App\Helpers\CommandParser;

class Badass
{
	private static $_VERSION = '1.0';

	private $_commands;
	private $_arguments;

	public function __construct($arguments)
	{
		$this->_loadCommands();
		$this->_arguments = $arguments;
	}

	public function run()
	{
		if (count($this->_arguments) < 2)
			$this->_printUsage();

		CommandParser::instance()->setCommands($this->_commands);
		$command = CommandParser::instance()->detectCommand(array_slice($this->_arguments, 1));

		if ($command == null)
			$this->_printUsage();

		$command->parse();
		$command->execute();
		$command->printErrors();
	}

	private function _loadCommands()
	{
		$this->_commands = [];
		foreach (scandir(__COMMANDS_DIR) as $file)
		{
			preg_match("/^([a-zA-Z]+).php$/", $file, $matches);
			if (empty($matches))
				continue;

			$command = 'App\\Commands\\' . $matches[1];
			$this->_commands[$command::$verboseName] = $command;
		}

		uasort($this->_commands, function ($a, $b) {
			return $a::$position > $b::$position;
		});
	}

	private function _getCommand($arguments)
	{
		$help = (isset($arguments[1]) && $arguments[1] == 'help');

		$arguments = implode(' ', array_slice($arguments, $help ? 2 : 1));

		$availables = [];
		foreach ($this->_commands as $name => $class)
		{
			if (strpos($arguments, $name) === 0)
				$availables[] = $name;
		}

		if (empty($availables))
			return null;

		usort($availables, function ($a, $b) {
			return strlen($b) - strlen($a);
		});

		$command = $this->_commands[$availables[0]];
		$commandArguments = array_values(array_filter(explode(' ', substr($arguments, strlen($availables[0])))));

		return new $command($commandArguments, $help);
	}

	private function _printUsage()
	{
		Shout::blue('Badass Utilities')->text(' version ')->green(static::$_VERSION)->jump();

		Shout::yellow('Usage:')->next();
		Shout::text('  command <arguments> [options]')->jump();

		Shout::yellow('Available commands:')->next();
        $this->_printCommands();
        Shout::next();
		die();
	}

    private function _printCommands()
    {
    	$deltaSpace = 0;
    	foreach($this->_commands as $command)
    	{
    		$commandName = $command::$verboseName;
    		$deltaSpace = $deltaSpace < strlen($commandName) ? strlen($commandName) : $deltaSpace;
    	}
    	$deltaSpace = $deltaSpace + 8;

        foreach($this->_commands as $command)
        {
        	$commandName = $command::$verboseName;

            Shout::green('  ' . strtolower($commandName));
            for($i = strlen($commandName); $i < $deltaSpace; ++$i)
				Shout::text(' ');
            Shout::text($command::$verboseDescription)->next();
        }
    }
}