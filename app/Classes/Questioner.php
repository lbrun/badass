<?php

namespace App\Classes;

class Questioner
{
	private static $instance;

	public static function instance()
	{
		if (self::$instance != null)
			return self::$instance;

		self::$instance = new Questioner();
		return self::$instance;
	}

	public static function __callStatic($method, $arguments)
    {
    	$method = '_' . $method;

    	if (!method_exists(self::instance(), $method))
    		die();

    	return call_user_func_array([self::instance(), $method], $arguments);
    }

	private function _ask($question)
	{
		Shout::next()->blue($question . '  ');

		$handle = fopen ('php://stdin', 'r');
		$line = fgets($handle);
		fclose($handle);

		return trim($line);
	}
}