<?php

return [

	'labels' => [

		'types' => [
			'feature' => 'Type: Feature',
			'optimization' => 'Type: Optimization',
			'bug' => 'Type: Bug',
		],

		'status' => [
			'working' => 'Status: In Progress',
			'review' => 'Status: Review Needed',
			'documentation' => 'Status: Documentation Needed'
		],
	],

	'status' => [
		'opened' => 'opened',
		'closed' => 'closed',
	],

	'prefix' => [
		'feature' => 'dev',
		'optimization' => 'opti',
		'bug' => 'fix',
		'misc' => 'misc',
	]

];